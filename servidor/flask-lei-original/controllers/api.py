#!/usr/bin/env python
# -*- coding: UTF-8 -*-

from flask import Blueprint, request, Response, session
from models import db, queries

apiRoute = Blueprint('api', __name__,  template_folder='views')

@apiRoute.route('/api/sparql', methods=['GET'])
def sparql():

    if request.method == "GET":
        try:
            if 'query' in session:
                query = session.get('query', None)
            else:
                query = request.args.get('query', None)
            
            limit = request.args.get('limit', None)

            formato = request.args.get('output', None)

            if formato not in ["csv", "json", "xml"]:
                formato = "json"

            if query:
                results = db.query(query, output=formato)
                if limit:
                    l=int(limit)+1
                    results = '\n'.join(results.split('\n')[:l])

                if formato=="csv":
                    mime="text/csv"
                elif formato=="xml":
                    mime="text/xml"
                elif formato=="json":
                    mime="application/json"
                return Response(results, mimetype=mime, headers={"Content-disposition":"attachment; filename=dados."+formato})
            else:
                return Response(status=404)
        except:
            return Response(status=404)
