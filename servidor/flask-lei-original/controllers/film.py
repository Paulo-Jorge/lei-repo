#!/usr/bin/env python
# -*- coding: UTF-8 -*-

from flask import Blueprint, render_template
from models import db, queries

filmRoute = Blueprint('film', __name__,  template_folder='views')

@filmRoute.route('/film')
def film():
    return render_template("film.html", title="Vídeo de apresentação")
